const container = document.querySelector("#container");
let allQuotes = [];

const quoteLimit = 5;
let startingIndex = 0;

const observer = new IntersectionObserver((entries) => {
    console.log(entries);
    entries.forEach((entry) => {
        if(!entry.isIntersecting) return
        if(entry.isIntersecting){
            observer.unobserve(entry.target);
             renderQuotes();
        };
    });
});

const formatQuotes = (quotes) => {
    let quoteStr = "";

    quotes.forEach((quote) => {
        quoteStr += `<div class="quote">${quote.text} - ${quote.author}</div>`;
       

    });

    return quoteStr;
};

const renderQuotes = () => {
    console.log(allQuotes.slice(0,5));

    const latestQuotes = formatQuotes(allQuotes.slice(startingIndex, startingIndex + quoteLimit)); //dynamic
    // console.log(getQuotes);
    container.innerHTML += latestQuotes;

    const renderedQuotes = document.querySelectorAll(".quote");
    startingIndex = renderQuotes.length;
    // console.log(renderedQuotes(renderedQuotes.length - 1));
    const lastRenderedQuote = renderedQuotes[renderedQuotes.length -1];
    observer.observe(lastRenderedQuote);

};

// to run the code

const loadAllQuotes =() => {
    fetch("https://type.fit/api/quotes")
    .then(res => res.json())
    .then((quotes) => {
        allQuotes = [...allQuotes, ...quotes];
        renderQuotes()
    });

};
loadAllQuotes();